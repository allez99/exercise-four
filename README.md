# Excercise four in Django #

This is the repository for luv.it test by Alejandro Lopez

### Minimu librares ###

* pip install django psycopg2
* pip install djangorestframework

### Migrations ###

The db is in postgresql. It only needs to create the User table, just run:

* ./manage.py makemigrations four
* ./manage.py migrate

Settings for localhost

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'luvit',
        'USER': 'luvituser',
        'PASSWORD': 'luvit',
        'HOST': 'localhost',
        'PORT': '',
    }
}


### Elastic Beanstalk ###

* 
* 
*