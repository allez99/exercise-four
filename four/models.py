from django.db import models
# Create your models here.

class User (models.Model):
    name = models.CharField(blank=False,max_length=250, error_messages={'blank': 'No olvides el nombre'})
    email = models.EmailField(unique=True, blank=False, max_length=50, error_messages={'invalid':'Este correo no es correcto','required': 'El email es obligatorio', 'unique': 'Este correo ya existe pruebe con otro', 'blank': 'No puede venir vacio'})
    password = models.CharField(max_length=100)
    date_created = models.DateTimeField(auto_now=True)