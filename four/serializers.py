from rest_framework import serializers
from .models import User
from django.contrib.auth.hashers import make_password


class UserSerializer(serializers.ModelSerializer):
    class User:
        model = User
        fields = ('id','name', 'email', 'password')

    class Meta:
        model = User
        fields = ('id','name', 'email', 'password')

    def create(self, validated_data):
        user = User(**validated_data)
        user.password=make_password(validated_data['password'])
        user.save()
        return user